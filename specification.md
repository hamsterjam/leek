LEEK16 Architecture
===================

Notation
--------

Throughout this document, registers are denoted by a lower case r followed by the register number or name.
This notation is used with variables as well.
For example r1 refers to register 1, rPC refers to the program counter register, rA refers to the register A.

To refer to a specific bit of a value, we use braces to indicate the bit number.
For example r1{3} is bit 3 of register 1, A{6} is bit 6 of value A, and rPC{A} is bit A of the program counter.

Similarly, a range of bits can be specified with two numbers in the braces
For example r1{0, 7} is the least significant 8 bits of register 1, and rPC{3, 6} is bits 3 through 6 of the program counter.

Flags are given their own notation denoted by a lower case f followed by the flag name or the corresponding bit in rFLG.
For example fCARRY is rFLG{2}, f5 is rFLG{5}, and fA is rFLG{A}.

We often need to use a register as a pointer to data in memory.
Any expression surrounded in square brackets is taken to mean "the value in memory with address given by the expression".
For example [34] is the the value at position 34 in memory, [rPC] is the value pointed to by the program counter, that is the current instruction.

Occasionally we need to refer to a 32 bit number stored between two registers.
In these cases we write rA:rB to mean a 32 bit number with the contents of rA being the most significant 16 bits,
and the contents of rB being the least significant 16 bits.

Definitions
-----------

<dl>
    <dt> Byte </dt>
    <dd>
        A string of 8 bits. We use this definition to correspond with the modern definition of a byte.
        This does not correspond to the minimum addressable memory unit which is a word.
    </dd>
    <dt> Lower Byte </dt>
    <dd>
        A byte formed from the least significant 8 bits of a word.
    </dd>
    <dt> Upper Byte </dt>
    <dd>
        A byte formed from the most significant 8 bits of a word.
    </dd>
    <dt> Word </dt>
    <dd>
        A string of 16 bits. This is the minimum addressable memory unit.
    </dd>
</dl>

Registers
---------

A LEEK16 computer has 16 registers, each holding a full 16 bit word, that can all be addressed by any operation.
However, six of these registers have special behavior and may produce unexpected behavior if altered.
The system has no built in way of handling byte data, if you wish to manipulate bytes you must still use a full 16 bit word register.

| Number | Mnemonic | Name             | Description                                                  |
|--------|----------|------------------|--------------------------------------------------------------|
| 0      | ZERO     | Zero             | Always contains the constant value 0                         |
| 1      |          |                  | General purpose                                              |
| 2      |          |                  | General purpose                                              |
| 3      |          |                  | General purpose                                              |
| 4      |          |                  | General purpose                                              |
| 5      |          |                  | General purpose                                              |
| 6      |          |                  | General purpose                                              |
| 7      |          |                  | General purpose                                              |
| 8      |          |                  | General purpose                                              |
| 9      |          |                  | General purpose                                              |
| 10     |          |                  | General purpose                                              |
| 11     | AUX      | Auxiliary        | Used by S-Arith instructions for multi-word input and output |
| 12     | IP       | Interupt Pointer | Address in memory of the interrupt handler routine           |
| 13     | FLG      | Flag             | Flags set by arithmetic instruction and interrupts           |
| 14     | SP       | Stack Pointer    | Used in conjunction with PUSH and POP to make a stack        |
| 15     | PC       | Program Counter  | Address in memory of the next instruction to be executed     |

The FLG register is broken down further into individual bits.
Any bits not assigned to a flag should be treated as reserved, although in practice nothing will happen if you do use them.

| Number | Mnemonic | Name                  | Description                                                           |
|--------|----------|-----------------------|-----------------------------------------------------------------------|
| 0      | ZERO     | Zero                  | Set if the result of an arithmetic operation is 0                     |
| 1      | NEG      | Negative              | Set if the result of an arithmetic operation is negative              |
| 2      | CARRY    | Carry/Borrow          | Set if the result of an unsigned operation produces a carry or borrow |
| 3      | OVER     | Overflow              | Set if the result of a signed operation produces an overflow          |
| 4      |          |                       |                                                                       |
| 5      |          |                       |                                                                       |
| 6      | IC       | Interrupt control     | If set, interrupts will trigger the interrupt handler routine         |
| 7      | ISS      | Interrupt signal soft | Signifies that a software interrupt occured                           |
| 8      | IS0      | Interrupt signal 0    | Signifies that an interrupt occured on line 0                         |
| 9      | IS1      | Interrupt signal 1    | Signifies that an interrupt occured on line 1                         |
| 10     | IS2      | Interrupt signal 2    | Signifies that an interrupt occured on line 2                         |
| 11     | IS3      | Interrupt signal 3    | Signifies that an interrupt occured on line 3                         |
| 12     | IS4      | Interrupt signal 4    | Signifies that an interrupt occured on line 4                         |
| 13     | IS5      | Interrupt signal 5    | Signifies that an interrupt occured on line 5                         |
| 14     | IS6      | Interrupt signal 6    | Signifies that an interrupt occured on line 6                         |
| 15     | IS7      | Interrupt signal 7    | Signifies that an interrupt occured on line 7                         |

Instructions
------------

All intructions are one word long with instruction formats as given by the following table.

<table>
    <tr>
        <th> Format
        <th> F <th> E <th> D <th> C <th> B <th> A <th> 9 <th> 8
        <th> 7 <th> 6 <th> 5 <th> 4 <th> 3 <th> 2 <th> 1 <th> 0
    <tr>
        <th colspan="17"> Long Operations
    <tr>
        <th>
        <th colspan="4"> Op Code
        <th colspan="4"> Source A
        <th colspan="4"> Source B
        <th colspan="4"> Destination
    <tr>
        <th> Arith
        <td> 1
        <td colspan="3"> Op
        <td colspan="4"> A (Reg)
        <td colspan="4"> B (Reg)
        <td colspan="4"> C (Reg)
    <tr>
        <th> I-Arith
        <td> 0 <td> 0
        <td colspan="2"> Op
        <td colspan="4"> A (Reg)
        <td colspan="4"> B (Imm)
        <td colspan="4"> C (Reg)
    <tr>
        <th> S-Arith
        <td> 0 <td> 1 <td> 0
        <td colspan="1"> Op
        <td colspan="4"> A (Reg)
        <td colspan="4"> B (Reg)
        <td colspan="4"> C (Reg)
    <tr>
        <th> R-Arith
        <td> 0 <td> 1 <td> 1
        <td colspan="1"> Op
        <td colspan="8"> A (Imm)
        <td colspan="4"> B (Reg)
    <tr>
        <th colspan="17"> Short Operations
    <tr>
        <th>
        <th> 0 <th> 0 <th> 0 <th> 0
        <th colspan="4"> Op Code
        <th colspan="4"> Source
        <th colspan="4"> Destination
    <tr>
        <th> Misc
        <td> 0 <td> 0 <td> 0 <td> 0 <td> 0 <td> 0
        <td colspan="2"> Op
        <td colspan="4"> A (Reg)
        <td colspan="4"> B (Reg)
    <tr>
        <th> Flag
        <td> 0 <td> 0 <td> 0 <td> 0 <td> 0 <td> 1
        <td colspan="2"> Op
        <td colspan="4"> A (Imm)
        <td colspan="4"> B (Reg)
    <tr>
        <th> Write
        <td> 0 <td> 0 <td> 0 <td> 0 <td> 1 <td> 0
        <td colspan="2"> Op
        <td colspan="4"> A (Reg)
        <td colspan="4"> B (Mem)
    <tr>
        <th> Read
        <td> 0 <td> 0 <td> 0 <td> 0 <td> 1 <td> 1
        <td colspan="2"> Op
        <td colspan="4"> A (Mem)
        <td colspan="4"> B (Reg)
</table>

A summary of instructions by format follows

#### Arith (Arithmetic)

| Op | Full Op | Mnemonic | Description             | Summary                        |
|----|---------|----------|-------------------------|--------------------------------|
| 0  | 0x8     | ADD      | Addition                | rC := rA + rB                  |
| 1  | 0x9     | ADC      | Addition with carry     | rC := rA + rB + fCARRY         |
| 2  | 0xA     | SUB      | Subtraction             | rC := rA - rB                  |
| 3  | 0xB     | SBB      | Subtraction with borrow | rC := rA - rB - rCARRY         |
| 4  | 0xC     | ROT      | Bitwise rotation        | rC := rA << rB (with wrapping) |
| 5  | 0xD     | OR       | Bitwise or              | rC := rA OR rB                 |
| 6  | 0xE     | AND      | Bitwise and             | rC := rA AND rB                |
| 7  | 0xF     | XOR      | Bitwise exclusive or    | rC := rA XOR rB                |

#### I-Arith (Immediate Arithmetic)

| Op | Full Op | Mnemonic | Description                | Summary                       |
|----|---------|----------|----------------------------|-------------------------------|
| 1  | 0x1     | INC      | Addition with immediate    | rC := rA + B                  |
| 2  | 0x2     | DEC      | Subtraction with immediate | rC := rA - B                  |
| 3  | 0x3     | ROTI     | Rotation with immediate    | rC := rA << B (with wrapping) |

#### S-Arith (Slow Arithmetic)

| Op | Full Op | Mnemonic | Description    | Summary                                  |
|----|---------|----------|----------------|------------------------------------------|
| 0  | 0x4     | MUL      | Multiplication | rAUX:rC := rA * rB                       |
| 1  | 0x5     | DIV      | Division       | rC := rAUX:rA / rB; rAUX := rAUX:rA % rB |

#### R-Arith (Relative Arithmetic)

| Op | Full Op | Mnemonic | Description              | Summary       |
|----|---------|----------|--------------------------|---------------|
| 0  | 0x6     | REL+     | Forward relative address | rB := rPC + A |
| 1  | 0x7     | REL-     | Reverse relative address | rB := rPC - A |

#### Misc (Miscelaneous)

| Op | Full Op | Mnemonic | Description           | Summary                |
|----|---------|----------|-----------------------|------------------------|
| 0  | 0x00    | MOV      | Moves register values | rB := rA               |
| 1  | 0x01    | NOT      | Bitwise not           | rB := NOT rA           |
| 2  | 0x02    | INTER    | Software interrupt    | See interrupts section |
| 3  | 0x03    | WAIT     | Wait for interrupt    | See interrupts section |

#### Flag

| Op | Full Op | Mnemonic | Description       | Summary                                   |
|----|---------|----------|-------------------|-------------------------------------------|
| 0  | 0x04    | PRED     | Predicate on flag | Skip next instruction unless rB{A} is set |
| 1  | 0x05    | SET      | Set flag          | rB{A} := 1                                |
| 2  | 0x06    | CLR      | Clear flag        | rB{A} := 0                                |
| 3  | 0x07    | TOG      | Toggle flag       | rB{A} := ~rB{A}                           |

#### Write

| Op | Full Op | Mnemonic | Description      | Summary                     |
|----|---------|----------|------------------|-----------------------------|
| 0  | 0x08    | STORE    | Store a word     | [rB] := rA                  |
| 1  | 0x09    | STOREL   | Store lower byte | [rB]{0, 7} := rA{0, 7}      |
| 2  | 0x0A    | STOREU   | Store upper byte | [rB]{8, 15} := rA{0, 7}     |
| 3  | 0x0B    | PUSH     | Push to stack    | rSP := rSP + 1; [rSP] := rA |

#### Read

| Op | Full Op | Mnemonic | Description     | Summary                     |
|----|---------|----------|-----------------|-----------------------------|
| 0  | 0x0C    | LOAD     | Load a word     | rB := [rA]                  |
| 1  | 0x0D    | LOADL    | Load lower byte | rB{0, 7} := [rA]{0, 7}      |
| 2  | 0x0E    | LOADU    | Load upper byte | rB{0, 7} := [rA]{8, 15}     |
| 3  | 0x0F    | POP      | Pop from stack  | rB := [rSP]; rSP := rSP - 1 |

