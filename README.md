LEEK
====

### *L*ittle *E*ducational *E*lectronic *K*omputer

This is a toy 16 bit RISC machine.
It is not intended to solve any interesting computation problems, rather it is an opportunity for me to learn about processor design.
This project is inspired by the microcomputers of the 1970's such as the PDP-11.
I invisage it to eventually run a simple UNIX-like operating system.

Building
--------

This project has no dependencies, but it will require you to run it in a POSIX compliant terminal.
I have no intention of supporting Windows at this point in time.

To build it, simply run

```
$ make release
```

To test it I have included a simple "Hello, World!" program written in LEEK16 assembly.
You can assemble and run this program with the commands

```
$ ./leek-asm examples/hello.lk -o hello
$ ./leek-vm -f hello
```
