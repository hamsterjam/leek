#ifndef LEEK_CODES_HPP_DEFINED
#define LEEK_CODES_HPP_DEFINED

// Note: not using enum class because they don't implicitly cast to integers
namespace Op {
    enum Code{
        // Short Operations

        // Misc
        MOV    = 0x00,
        NOT    = 0x01,
        INTER  = 0x02,
        WAIT   = 0x03,
        // Flag
        PRED   = 0x04,
        SET    = 0x05,
        CLR    = 0x06,
        TOG    = 0x07,
        // Write
        STORE  = 0x08,
        STOREL = 0x09,
        STOREU = 0x0A,
        PUSH   = 0x0B,
        // Read
        LOAD   = 0x0C,
        LOADL  = 0x0D,
        LOADU  = 0x0E,
        POP    = 0x0F,

        // Long Operations

        // I-Arith
        INC    = 0x11,
        DEC    = 0x12,
        ROTI   = 0x13,
        // S-Arith
        MUL    = 0x14,
        DIV    = 0x15,
        // R-Arith
        RELP   = 0x16,
        RELN   = 0x17,
        // Arith
        ADD    = 0x18,
        ADC    = 0x19,
        SUB    = 0x1A,
        SBB    = 0x1B,
        ROT    = 0x1C,
        OR     = 0x1D,
        AND    = 0x1E,
        XOR    = 0x1F,

        INVALID = 0x100
    };

    Code fromString(const char* str);
}

namespace Reg {
    enum Code {
        ZERO = 0,
        AUX  = 11,
        IP   = 12,
        FLG  = 13,
        SP   = 14,
        PC   = 15,

        INVALID = 16
    };

    Code fromString(const char* str);
}

namespace Flag {
    enum Code {
        ZERO = 0,
        NEG  = 1,
        CARRY = 2,
        OVER  = 3,
        IC    = 6,
        ISS   = 7,
        IS0   = 8,
        IS1   = 9,
        IS2   = 10,
        IS3   = 11,
        IS4   = 12,
        IS5   = 13,
        IS6   = 14,
        IS7   = 15,

        INVALID = 16
    };

    Code fromString(const char* str);
}

#endif
