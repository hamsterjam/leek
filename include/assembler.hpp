#ifndef LEEK_ASSEMBLER_HPP_DEFINED
#define LEEK_ASSEMBLER_HPP_DEFINED

#include <cstdint>
#include "codes.hpp"

uint16_t assemble(Op::Code op, uint8_t op1, uint8_t op2, uint8_t op3);

#endif
