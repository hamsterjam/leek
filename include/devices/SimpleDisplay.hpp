#ifndef LEEK_DEVICES_SIMPLE_DISPLAY_HPP_DEFINED
#define LEEK_DEVICES_SIMPLE_DISPLAY_HPP_DEFINED

#include "IODevice.hpp"

class SimpleDisplay: public IODevice {
    protected:
        void onWake();
};

#endif
