#ifndef LEEK_DEVICES_INCREMENTER_HPP_DEFINED
#define LEEK_DEVICES_INCREMENTER_HPP_DEFINED

#include "IODevice.hpp"

class Incrementer: public IODevice {
    protected:
        void onWake();
};

#endif
