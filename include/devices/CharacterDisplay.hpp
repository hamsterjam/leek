#ifndef LEEK_CHARACTER_DISPLAY_HPP_DEFINED
#define LEEK_CHARACTER_DISPLAY_HPP_DEFINED

#include <cstdint>
#include "IODevice.hpp"

// NOTE:
//      This will only work on unix systems (for now). If you are on windows...
//      go install linux or something.

class CharacterDisplay: public IODevice {
    public:
        CharacterDisplay();

        void onWake();

    private:
        uint16_t buffer[2000] = {0x0F00};

        void redraw();
};

#endif
