#ifndef LEEK_I_O_DEVICE_HPP_DEFINED
#define LEEK_I_O_DEVICE_HPP_DEFINED

#include <thread>
#include <mutex>
#include <condition_variable>

#include <cstdint>

class VirtualMachine;

class IODevice {
    public:
        IODevice();
        ~IODevice();

        void alert(std::size_t addr);

    private:
        std::thread th;
        std::mutex  mu;
        std::condition_variable cv;
        bool wake = false;
        bool kill = false;

        friend VirtualMachine;
        VirtualMachine* vm;
        uint16_t*       mem;
        uint16_t        line;

        std::size_t ignoreFirst = 0;
        std::size_t ignoreLast  = 0;

    protected:
        void interrupt();
        uint16_t& at(std::size_t addr);

        void ignoreRange(std::size_t first, std::size_t last);

        virtual void onWake() = 0;
};

#endif
