#ifndef LEEK_VIRTUAL_MACHINE_HPP_DEFINED
#define LEEK_VIRTUAL_MACHINE_HPP_DEFINED

#include <cstdint>

#include <map>
#include <initializer_list>
#include <istream>

#include <mutex>
#include <condition_variable>

#include "codes.hpp"

class IODevice;

class VirtualMachine {
    public:
        void execute(uint16_t op);
        void tick();

        void interrupt(uint16_t line);

        uint16_t peekReg(Reg::Code regID);
        uint16_t peekMem(uint16_t addr);
        bool     peekFlag(Flag::Code flag);

        void addDevice(IODevice& dev, uint16_t first, uint16_t last, uint16_t line);
        void initMem(std::istream& in);
        void initMem(std::initializer_list<uint16_t> in);

    private:
        uint16_t reg[0x10] = {0};
        uint16_t mem[0x10000] = {0};

        std::mutex intrMu;
        std::condition_variable intrCv;
        bool hardIntr[0x10] = {false};
        bool softIntr = false;
        bool intrAny = false;

        std::map<uint16_t, IODevice*> devs;

        void setFlag(Flag::Code flag, bool val);
};

#endif
