#ifndef LEEK_TEST_HPP_DEFINED
#define LEEK_TEST_HPP_DEFINED

#include <iostream>
#include <iomanip>

#ifndef MESSAGE_LENGTH
#define MESSAGE_LENGTH 30
#endif

unsigned long TEST_firstfail;
unsigned long TEST_asserts;
unsigned long TEST_successes;

#define startTest(message) { \
    std::cout << std::left << std::setw(MESSAGE_LENGTH) << (message) << std::flush; \
    TEST_asserts = 0; \
    TEST_successes = 0; \
    TEST_firstfail = 0; \
}

#define testAssert(expr) { \
    bool res = (expr); \
    TEST_asserts += 1; \
    TEST_successes += res ? 1 : 0; \
    if (!res && !TEST_firstfail) TEST_firstfail = __LINE__; \
}

#define endTest() { \
    double percentage = 100.0 * (double) TEST_successes / (double) TEST_asserts; \
    std::cout << std::setprecision(3) << percentage << "%" << std::endl; \
    if (TEST_successes != TEST_asserts) { \
        std::cerr << __FILE__; \
        std::cerr << ":" << TEST_firstfail; \
        std::cerr << ":" << " First failing assert here" << std::endl; \
    } \
}


#endif
