#define MESSAGE_LENGTH 40
#include "test.hpp"

#include "VirtualMachine.hpp"
#include "codes.hpp"

#include <random>
#include <sstream>
#include <iomanip>


uint16_t rand16(uint16_t min = 0, uint16_t max = 0xFFFF) {
    static std::random_device rd;
    static std::default_random_engine gen(rd());
    std::uniform_int_distribution<uint16_t> dist(min, max);

    return dist(gen);
}

void setFlag(VirtualMachine& vm, Flag::Code flg, bool value) {
    uint16_t instr;
    if (value) instr = 0x000D | Op::SET << 8;
    else       instr = 0x000D | Op::CLR << 8;
    instr |= (flg & 0xF) << 4;

    vm.execute(instr);
}

void clearMem(VirtualMachine& vm) {
    // Set the PC to 0
    vm.execute(0x000F |  Op::MOV << 8);  // MOV r0 rPC

    vm.initMem({
        0x0061 | (Op::INC & 0xF)  << 12, // 0: INC  r0  6 r1
        0x0111 | (Op::INC & 0xF)  << 12, // 1: INC  r1  1 r1
        0x0100 | (Op::ADD & 0xF)  << 12, // 2: ADD  r1 r0 r0
        0x000D |  Op::PRED        <<  8, // 2: PRED fZERO
        0x001F | (Op::RELN & 0xF) << 12, // 3: HALT
        0x0001 |  Op::STORE       <<  8, // 4: STORE r0 r1
        0x006F | (Op::RELN & 0xF) << 12  // 5: JMP- 6
    });

    // Run untill we halt
    uint16_t prev = 0xFFFF;
    while (vm.peekReg(Reg::PC) != prev) {
        prev = vm.peekReg(Reg::PC);
        vm.tick();
    }

    // Clear the first 6 cells
    vm.execute(0x000F | Op::MOV << 8);  // MOV r0 rPC
    vm.initMem({0, 0, 0, 0, 0, 0, 0});
}

void fillMemWithAddress(VirtualMachine& vm, Op::Code op) {
    // Set the PC back to 0
    vm.execute(0x000F |  Op::MOV << 8);          // MOV r0 rPC

    uint16_t instr = 0x011 | op << 8;

    vm.initMem({
        0x0061 | (Op::INC & 0xF)  << 12, // 0: INC  r0  6 r1
        0x0111 | (Op::INC & 0xF)  << 12, // 1: INC  r1  1 r1
        0x0100 | (Op::ADD & 0xF)  << 12, // 2: ADD  r1 r0 r0
        0x000D |  Op::PRED        <<  8, // 2: PRED fZERO
        0x001F | (Op::RELN & 0xF) << 12, // 3: HALT
        instr,                           // 4: STORE r1 r1
        0x006F | (Op::RELN & 0xF) << 12  // 5: JMP- 6
    });

    // Run untill we halt
    uint16_t prev = 0xFFFF;
    while (vm.peekReg(Reg::PC) != prev) {
        prev = vm.peekReg(Reg::PC);
        vm.tick();
    }
}

uint16_t doLongOp(VirtualMachine& vm, Op::Code op, uint16_t inA, uint16_t inB) {
    uint16_t instr;
    instr  = (op & 0xF) << 12;
    instr |= 0x0123;

    vm.initMem({
        0x0041 | (Op::RELP & 0xF) << 12, // REL+  4 r1
        0x0042 | (Op::RELP & 0xF) << 12, // REL+  4 r2
        0x0011 |  Op::LOAD        <<  8, // LOAD r1 r1
        0x0022 |  Op::LOAD        <<  8, // LOAD r2 r2
        instr,
        inA,
        inB
    });
    for (int i = 0; i < 5; ++i) vm.tick();

    return vm.peekReg((Reg::Code) 3);
}

uint16_t doLongImmediateOp(VirtualMachine& vm, Op::Code op, uint16_t inA, uint16_t inB) {
    uint16_t instr;
    instr  = (op  & 0xF) << 12;
    instr |= (inB & 0xF) <<  4;
    instr |= 0x0103;

    vm.initMem({
        0x0021 | (Op::RELP & 0xF) << 12, // REL+  2 r1
        0x0011 |  Op::LOAD        <<  8, // LOAD r1 r1
        instr,
        inA
    });
    for (int i = 0; i < 3; ++i) vm.tick();

    return vm.peekReg((Reg::Code) 3);
}

void verifyStateFlags(VirtualMachine& vm, uint16_t out) {
    int16_t s_out = (int16_t) out;
    testAssert((s_out == 0) == vm.peekFlag(Flag::ZERO));
    testAssert((s_out <  0) == vm.peekFlag(Flag::NEG));
}

int main(int argc, char** argv) {
    VirtualMachine vm;

    { // Move
        startTest("Testing move operations...");
        for (uint16_t dest = 1; dest < 16; ++dest) {
            for (uint16_t src = 1; src < 16; ++src) {
                if (dest == src) continue;
                uint16_t value = 0x0000;
                do {
                    // Set dest to all 1s (doing this first because NOT sets state flags)
                    vm.execute(dest | Op::MOV << 8);             // MOV r0    rDEST
                    vm.execute(dest | dest << 4 | Op::NOT << 8); // NOT rDEST rDEST

                    uint16_t temp;
                    if      (src != 1 && temp != 1) temp = 1;
                    else if (src != 2 && temp != 2) temp = 2;
                    else                            temp = 3;

                    // Load value into the source register
                    vm.initMem({
                        (uint16_t) (0x0010 | temp |  (Op::RELP & 0xF) << 12), // REL+     1 rTEMP
                        (uint16_t) (src | temp << 4 | Op::LOAD        <<  8), // LOAD rTEMP rSRC
                        value
                    });
                    vm.tick(); vm.tick();

                    // MOV src into dest
                    vm.execute(dest | src << 4 | Op::MOV << 8); // MOV rSRC rDEST
                    testAssert(vm.peekReg((Reg::Code) dest) == value);

                    // Clear dest, then try the MOV again
                    vm.execute(dest | Op::MOV << 8);            // MOV r0   rDEST
                    vm.execute(dest | src << 4 | Op::MOV << 8); // MOV rSRC rDEST
                    testAssert(vm.peekReg((Reg::Code) dest) == value);
                } while(++value != 0);
            }
        }
        endTest();
    }
    { // Addition
        startTest("Testing addition...");
        auto verify = [&vm](uint16_t inA, uint16_t inB, bool carry, uint16_t out) {
            uint16_t correctOut = inA + inB + (carry ? 1 : 0);

            testAssert(correctOut == out);
        };
        auto verifyCarry = [&vm](uint16_t inA, uint16_t inB, uint16_t out) {
            bool carry = out < inA;
            testAssert(carry == vm.peekFlag(Flag::CARRY));
        };
        auto verifyOverflow = [&vm](uint16_t inA, uint16_t inB, uint16_t out) {
            int16_t s_inA = (int16_t) inA;
            int16_t s_inB = (int16_t) inB;
            int16_t s_out = (int16_t) out;

            bool overflow = false;
            if (s_inA > 0 && s_inB > 0 && s_out < 0) overflow = true;
            if (s_inA < 0 && s_inB < 0 && s_out > 0) overflow = true;

            testAssert(overflow == vm.peekFlag(Flag::OVER));
        };
        for (int i = 0; i < 1e6; ++i) {
            { // ADD instruction
                uint16_t inA = rand16();
                uint16_t inB = rand16();
                uint16_t out = doLongOp(vm, Op::ADD, inA, inB);
                verify(inA, inB, false, out);
                verifyCarry(inA, inB, out);
                verifyOverflow(inA, inB, out);
                verifyStateFlags(vm, out);
            }
            { // ADC intruction
                bool carry = rand16(0, 1) == 1; //TODO// This is bad
                setFlag(vm, Flag::CARRY, carry);

                uint16_t inA = rand16();
                uint16_t inB = rand16();
                uint16_t out = doLongOp(vm, Op::ADC, inA, inB);
                verify(inA, inB, carry, out);
                verifyCarry(inA, inB, out);
                verifyStateFlags(vm, out);
            }
            { // INC instruction
                uint16_t inA = rand16();
                uint16_t inB = rand16(0x0, 0xF);
                uint16_t out = doLongImmediateOp(vm, Op::INC, inA, inB);
                verify(inA, inB, false, out);
            }
        }
        endTest();
    }
    { // Subtraction
        startTest("Testing subtraction...");
        auto verify = [&vm](uint16_t inA, uint16_t inB, bool carry, uint16_t out) {
            uint16_t correctOut = inA - inB - (carry ? 1 : 0);

            testAssert(correctOut == out);
        };
        auto verifyBorrow = [&vm](uint16_t inA, uint16_t inB, uint16_t out) {
            bool borrow = out > inA;
            testAssert(borrow == vm.peekFlag(Flag::CARRY));
        };
        auto verifyOverflow = [&vm](uint16_t inA, uint16_t inB, uint16_t out) {
            int16_t s_inA = (int16_t) inA;
            int16_t s_inB = (int16_t) inB;
            int16_t s_out = (int16_t) out;

            bool overflow = false;
            if (s_inA > 0 && s_inB < 0 && s_out < 0) overflow = true;
            if (s_inA < 0 && s_inB > 0 && s_out > 0) overflow = true;

            testAssert(overflow == vm.peekFlag(Flag::OVER));
        };
        for (int i = 0; i < 1e6; ++i) {
            { // SUB instruction
                uint16_t inA = rand16();
                uint16_t inB = rand16();
                uint16_t out = doLongOp(vm, Op::SUB, inA, inB);
                verify(inA, inB, false, out);
                verifyBorrow(inA, inB, out);
                verifyOverflow(inA, inB, out);
                verifyStateFlags(vm, out);
            }
            { // SBB instruction
                bool carry = rand16(0, 1) == 1; //TODO// This is bad
                setFlag(vm, Flag::CARRY, carry);

                uint16_t inA = rand16();
                uint16_t inB = rand16();
                uint16_t out = doLongOp(vm, Op::SBB, inA, inB);
                verify(inA, inB, carry, out);
                verifyBorrow(inA, inB, out);
                verifyStateFlags(vm, out);
            }
            { // DEC instruction
                uint16_t inA = rand16();
                uint16_t inB = rand16(0x0, 0xF);
                uint16_t out = doLongImmediateOp(vm, Op::DEC, inA, inB);
                verify(inA, inB, false, out);
            }
        }
        endTest();
    }
    { // Multiplication
        startTest("Testing multiplication...");
        for (int i = 0; i < 1e6; ++i) {
            uint16_t inA = rand16();
            uint16_t inB = rand16();

            uint16_t outLo = doLongOp(vm, Op::MUL, inA, inB);
            uint16_t outHi = vm.peekReg(Reg::AUX);
            uint32_t out = outHi << 16 | outLo;

            uint32_t correctOut = (uint32_t) inA * (uint32_t) inB;

            testAssert(out == correctOut);
            // Shouldn't touch flags
        }
        endTest();
    }
    { // Division
        startTest("Testing division...");
        for (int i = 0; i < 1e6; ++i) {
            uint16_t inALo = rand16();
            uint16_t inAHi = rand16(0, 0xFFFE);

            uint32_t inA = inAHi << 16 | inALo;
            uint16_t inB = rand16(inAHi + 1);

            // Write inAHi to rAUX
            vm.initMem({
                0x001B | (Op::RELP & 0xF) << 12, // REL+    1 rAUX
                0x00BB |  Op::LOAD        <<  8, // LOAD rAUX rAUX
                inAHi
            });
            vm.tick(); vm.tick();

            uint16_t qOut = doLongOp(vm, Op::DIV, inALo, inB);
            uint16_t rOut = vm.peekReg(Reg::AUX);

            uint16_t qCorrectOut = inA / inB;
            uint16_t rCorrectOut = inA % inB;

            testAssert(qOut == qCorrectOut);
            testAssert(rOut == rCorrectOut);
        }
        endTest();
    }
    { // Rotation
        startTest("Testing rotation...");
        auto verify = [&vm](uint16_t inA, uint16_t inB, uint16_t out) {
            inB %= 16;
            uint16_t mask = (1 << inB) - 1;
            uint16_t correctOut;
            correctOut  = ~mask & (inA << inB);
            correctOut |=  mask & (inA >> (16 - inB));

            testAssert(correctOut == out);
            verifyStateFlags(vm, correctOut);
        };
        for (int i = 0; i < 1e6; ++i) {
            { // ROT instruction
                uint16_t inA = rand16();
                uint16_t inB = rand16();
                verify(inA, inB, doLongOp(vm, Op::ROT, inA, inB));
            }
            { // ROTI instruction
                uint16_t inA = rand16();
                uint16_t inB = rand16(0x0, 0xF);
                verify(inA, inB, doLongImmediateOp(vm, Op::ROTI, inA, inB));
            }

        }
        endTest();
    }
    { // Bitwise logic
        startTest("Testing bitwise logic...");
        for (int i = 0; i < 1e6; ++i) {
            uint16_t inA = rand16();
            uint16_t inB = rand16();

            { // OR
                uint16_t out = doLongOp(vm, Op::OR, inA, inB);
                testAssert((inA | inB) == out);
                verifyStateFlags(vm, out);
            }
            { // AND
                uint16_t out = doLongOp(vm, Op::AND, inA, inB);
                testAssert((inA & inB) == out);
                verifyStateFlags(vm, out);
            }
            { // XOR
                uint16_t out = doLongOp(vm, Op::XOR, inA, inB);
                testAssert((inA ^ inB) == out);
                verifyStateFlags(vm, out);
            }
            { // NOT
                vm.execute(0x0013 | Op::NOT << 8); // NOT r1 r3 (r1 is still inA)
                uint16_t out = vm.peekReg((Reg::Code) 3);
                testAssert((uint16_t) ~inA == out); // FUCKING C++
                verifyStateFlags(vm, out);
            }
        }
        endTest();
    }
    { // Relative addressing
        startTest("Testing relative addressing...");
        for (int i = 0; i < 1e6; ++i) {
            uint16_t inA = rand16();
            uint16_t inB = rand16(0x00, 0xFF);

            // Set rPC to inA
            vm.initMem({
                0x0011 | (Op::RELP & 0xF) << 12, // REL+  1 r1
                0x001F |  Op::LOAD        <<  8, // LOAD r1 rPC
                inA
            });
            vm.tick(), vm.tick();

            { // REL+
                uint16_t instr;
                instr  = 0x0001 | (Op::RELP & 0xF) << 12;
                instr |= inB << 4;

                vm.execute(instr);
                uint16_t out = vm.peekReg((Reg::Code) 1);
                uint16_t correctOut = inA + inB;
                testAssert(out == correctOut);
            }
            { // REL-
                uint16_t instr;
                instr  = 0x0001 | (Op::RELN & 0xF) << 12;
                instr |= inB << 4;

                vm.execute(instr);
                uint16_t out = vm.peekReg((Reg::Code) 1);
                uint16_t correctOut = inA - inB;
                testAssert(out == correctOut);
            }
        }
        endTest();
    }
    { // Flag manipulators
        startTest("Testing flag manipulators...");
        for (uint16_t reg = 1; reg < 16; ++reg) { // Skipping rZERO
            for (uint16_t bit = 0; bit < 16; ++bit) {
                uint16_t mask = 1 << bit;
                uint16_t suffix = bit << 4 | reg;
                // Clear the register
                vm.execute(reg | Op::MOV << 8); // MOV r0 rReg

                // Doing it twice to make sure setting a set bit works
                for (int i = 0; i < 2; ++i) {
                    vm.execute(suffix | Op::SET << 8); // SET bit rReg
                    testAssert(vm.peekReg((Reg::Code) reg) == mask);
                }

                vm.execute(suffix | Op::TOG << 8); // TOG bit rReg
                testAssert(vm.peekReg((Reg::Code) reg) == 0);

                // Set the register to 0xFFFF
                vm.execute(reg | reg << 4 | Op::NOT << 8);             // NOT rReg rReg
                vm.execute(Reg::FLG | Flag::ZERO << 4 | Op::SET << 8); // SET fZERO

                // Again, clear twice to check clearing a cleared bit
                for (int i = 0; i < 2; ++i) {
                    vm.execute(suffix | Op::CLR << 8); // CLR bit rReg
                    testAssert(vm.peekReg((Reg::Code) reg) == (uint16_t) ~mask);
                }

                vm.execute(suffix | Op::TOG << 8); // TOG bit rReg
                testAssert(vm.peekReg((Reg::Code) reg) == 0xFFFF);
            }
        }
        endTest();
    }
    { // Conditional execution
        startTest("Testing conditional execution...");
        for (uint16_t reg = 1; reg < 15; ++reg) { // Skipping rZERO and rPC
            for (uint16_t bit = 0; bit < 16; ++bit) {
                // Set bit to be the only high bit in reg
                vm.execute(reg | Op::MOV << 8);            // MOV r0 rReg
                vm.execute(reg | bit << 4 | Op::SET << 8); // SET bit rReg

                // Make sure we start from 0
                vm.execute(0x000F | Op::MOV << 8); // MOV r0 rPC
                uint16_t instr = reg | bit << 4 | Op::PRED << 8;
                vm.initMem({instr});
                vm.tick();

                // Assert that we did not skip
                testAssert(vm.peekReg(Reg::PC) == 1);

                // Set bit to be the only low bit in reg
                vm.execute(reg | reg << 4 | Op::NOT << 8); // NOT rReg rReg
                vm.execute(0x00D0 | Op::SET);              // SET fZERO
                vm.execute(reg | bit << 4 | Op::CLR << 8); // CLR bit rReg

                // Start from 0 again, mem should already be set
                vm.execute(0x000F | Op::MOV << 8); // MOV r0 rPC
                vm.tick();

                // Assert that we did skip
                testAssert(vm.peekReg(Reg::PC) == 2);
            }
        }
        endTest();
    }
    { // Memory storage
        startTest("Testing memory storage...");

        fillMemWithAddress(vm, Op::STORE);
        for (uint16_t i = 0x0007; i != 0; ++i) {
            testAssert(vm.peekMem(i) == i);
        }

        clearMem(vm);
        fillMemWithAddress(vm, Op::STOREL);
        for (uint16_t i = 0x0007; i != 0; ++i) {
            testAssert(vm.peekMem(i) == (i & 0x00FF));
        }

        clearMem(vm);
        fillMemWithAddress(vm, Op::STOREU);
        for (uint16_t i = 0x0007; i != 0; ++i) {
            testAssert(vm.peekMem(i) == (i & 0xFF00) >> 8);
        }

        endTest();
    }
    { // Memory retrieval
        startTest("Testing memory retrieval...");

        fillMemWithAddress(vm, Op::STORE);

        // We use the first 5 address for the load program
        for (uint16_t i = 0x0007; i != 0; ++i) {
            vm.execute(0x000F |  Op::MOV << 8);          // MOV  r0 rPC
            vm.execute(0x0041 | (Op::INC & 0xF) << 12);  // INC r0 4 r1
            vm.initMem({
                0x0011 | Op::LOAD  << 8, // LOAD  r1 r1
                0x0012 | Op::LOADL << 8, // LOADL r1 r2
                0x0013 | Op::LOADU << 8, // LOADU r1 r3
                0x0011 | Op::LOAD  << 8, // LOAD  r1 r1
                i
            });
            for (int j = 0; j < 4; ++j) vm.tick();

            testAssert(vm.peekReg((Reg::Code) 1) == i);
            testAssert(vm.peekReg((Reg::Code) 2) == (i & 0x00FF));
            testAssert(vm.peekReg((Reg::Code) 3) == (i & 0xFF00) >> 8)
        }
        endTest();
    }
    { // Memory stack operationS
        startTest("Testing memory stack operations...");

        // Fill the memory, but using push
        clearMem(vm);
        vm.execute(0x000F |  Op::MOV << 8);          // MOV  r0 rPC
        vm.execute(0x004E | (Op::INC & 0xF) << 12);  // INC r0 4 rSP
        vm.initMem({
            0x00EE |  Op::PUSH        <<  8, // PUSH rSP
            0x0E00 | (Op::ADD  & 0xF) << 12, // ADD  rSP r0 r0
            0x000D |  Op::PRED        <<  8, // PRED fZERO
            0x001F | (Op::RELN & 0xF) << 12, // HALT
            0x005F | (Op::RELN & 0xF) << 12  // JMP- 5
        });

        // Run untill we halt
        uint16_t prev = 0xFFFF;
        while (vm.peekReg(Reg::PC) != prev) {
            prev = vm.peekReg(Reg::PC);
            vm.tick();
        }

        // Pop them and check we get the values back (in reverse order)
        for (uint16_t i = 0xFFFF; i > 5; --i) {
            vm.execute(0x00E1 | Op::POP << 8); // POP r1
            testAssert(vm.peekReg((Reg::Code) 1) == i);
        }
        testAssert(true);

        endTest();
    }
    { // Fibonacci
        startTest("Testing fibonacci sequence...");

        const uint16_t fib[] = {
               1,    1,     2,     3,     5,     8,
              13,   21,    34,    55,    89,   144,
             233,  377,   610,   987,  1597,  2584,
            4181, 6765, 10946, 17711, 28657, 46368
        };

        vm.initMem({
            0x00BE | (Op::RELP & 0xF) << 12,    // REL+     11      rSP
            0x0011 | (Op::INC & 0xF)  << 12,    // INC      r0      1       r1
            0x0002 |  Op::MOV         <<  8,    // MOV      r0      r2
            0x00C3 | (Op::INC & 0xF)  << 12,    // INC      r0      12      r3
            0x0121 | (Op::ADD & 0xF)  << 12,    // ADD      r1      r2      r1
            0x001E |  Op::PUSH        <<  8,    // PUSH     r1      rSP
            0x0122 | (Op::ADD  & 0xF) << 12,    // ADD      r1      r2      r2
            0x002E |  Op::PUSH        <<  8,    // PUSH     r2      rSP
            0x0313 | (Op::DEC & 0xF)  << 12,    // DEC      r3      1       r3
            0x0300 | (Op::ADD & 0xF)  << 12,    // ADD      r1      r0      r0
            0x000D |  Op::PRED        <<  8,    // PRED     fZERO   rFLG
            0x001F | (Op::RELN & 0xF) << 12,    // REL-     1       rPC
            0x009F | (Op::RELN & 0xF) << 12,    // REL-     9       rPC
        });

        // Run till halt
        uint16_t prev = vm.peekReg(Reg::PC) + 1;
        while (prev != vm.peekReg(Reg::PC)) {
            prev = vm.peekReg(Reg::PC);
            vm.tick();
        }

        for (int i = 23; i >= 0; --i) {
            vm.execute(0x00E1 | Op::POP << 8); // POP rSP r1
            testAssert(vm.peekReg((Reg::Code) 1) == fib[i]);
        }

        endTest();
    }

     return 0;
}
