NAME = leek

#Local Folders
SOURCE_DIR = source
HEADER_DIR = include
OBJECT_DIR = object

#Find all the sources (recursively)
CPP_PATHS = $(wildcard $(SOURCE_DIR)/*.cpp) $(wildcard $(SOURCE_DIR)/**/*.cpp)
CPP_FILES = $(CPP_PATHS:$(SOURCE_DIR)/%=%)
OBJECTS = $(filter-out %main.o,$(CPP_FILES:.cpp=.o))

MAIN_PATHS = $(wildcard $(SOURCE_DIR)/*main.cpp)
MAIN_NAMES = $(addprefix $(NAME)-, $(MAIN_PATHS:$(SOURCE_DIR)/%-main.cpp=%))

#Globals
CFLAGS = -std=c++11
LFLAGS = -I$(HEADER_DIR) -lpthread

#Debug Varibales
DEBUG_TARGET = $(NAME)_debug
DEBUG_CFLAGS = $(CFLAGS) -g
DEBUG_LFLAGS = $(LFLAGS)
DEBUG_OBJECT_DIR = $(OBJECT_DIR)/debug
DEBUG_OBJECTS = $(addprefix $(DEBUG_OBJECT_DIR)/, $(OBJECTS))
DEBUG_MAIN_NAMES = $(addsuffix _debug, $(MAIN_NAMES))

#Release Variables
RELEASE_TARGET = $(NAME)
RELEASE_CFLAGS = $(CFLAGS) -O3
RELEASE_LFLAGS = $(LFLAGS)
RELEASE_OBJECT_DIR = $(OBJECT_DIR)/release
RELEASE_OBJECTS = $(addprefix $(RELEASE_OBJECT_DIR)/, $(OBJECTS))
RELEASE_MAIN_NAMES = $(MAIN_NAMES)

default: debug

clean:
	@rm -rf $(OBJECT_DIR)
	@rm -f *-test
	@rm -f $(NAME)-*

pre_pre:
	@[ -d $(OBJECT_DIR) ] || mkdir $(OBJECT_DIR)
	@[ -d temp ] || mkdir temp
	@cd $(SOURCE_DIR); \
	find . -type d -exec mkdir -p ../temp/{} \;

pre_debug: pre_pre
	@[ -d $(DEBUG_OBJECT_DIR) ] || mkdir $(DEBUG_OBJECT_DIR)
	@find temp -not -empty -exec cp -r temp/* $(DEBUG_OBJECT_DIR) \;
	@rm -r temp

pre_release: pre_pre
	@[ -d $(RELEASE_OBJECT_DIR) ] || mkdir $(RELEASE_OBJECT_DIR)
	@find temp -not -empty -exec cp -r temp/* $(RELEASE_OBJECT_DIR) \;
	@rm -r temp

debug: pre_debug $(DEBUG_OBJECTS) $(DEBUG_MAIN_NAMES)

release: pre_release $(RELEASE_OBJECTS) $(RELEASE_MAIN_NAMES)

$(NAME)-%_debug: $(SOURCE_DIR)/%-main.cpp $(DEBUG_OBJECTS)
	@echo 'Compiling debug build of $@...'
	@$(CXX) $(DEBUG_CFLAGS) $^ -o $@ $(DEBUG_LFLAGS)

$(NAME)-%: $(SOURCE_DIR)/%-main.cpp $(RELEASE_OBJECTS)
	@echo 'Compiling release build of $@...'
	@$(CXX) $(RELEASE_CFLAGS) $^ -o $@ $(RELEASE_LFLAGS)

%-test: test/%.cpp pre_debug $(DEBUG_OBJECTS)
	@echo 'Compiling test build...'
	@$(CXX) $(DEBUG_CFLAGS) $(DEBUG_OBJECTS) $< -o $@ $(DEBUG_LFLAGS)

$(DEBUG_OBJECT_DIR)/%.o : $(SOURCE_DIR)/%.cpp
	@echo 'Compiling '$@'...'
	@$(CXX) $(DEBUG_CFLAGS) -c $< -o $@ $(DEBUG_LFLAGS)

$(RELEASE_OBJECT_DIR)/%.o : $(SOURCE_DIR)/%.cpp
	@echo 'Compiling '$@'...'
	@$(CXX) $(RELEASE_CFLAGS) -c $< -o $@ $(RELEASE_LFLAGS)

