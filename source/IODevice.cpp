#include "IODevice.hpp"

#include <thread>
#include <mutex>
#include <condition_variable>

#include <cstdint>

#include "VirtualMachine.hpp"

IODevice::IODevice() {
    auto work = [this] {
        std::unique_lock<std::mutex> lk(mu);

        while (1) {
            cv.wait(lk, [this]{return wake;});
            if (kill) break;
            wake = false;
            onWake();
        }
        lk.unlock();
    };

    th = std::thread(work);
}

IODevice::~IODevice() {
    {
        std::lock_guard<std::mutex> lk(mu);
        kill = true;
        wake = true;
    }
    cv.notify_all();
    th.join();
}

void IODevice::alert(std::size_t addr) {
    if (addr >= ignoreFirst && addr < ignoreLast) return;

    std::lock_guard<std::mutex> lk(mu);
    wake = true;
    cv.notify_all();
}

void IODevice::interrupt() {
    vm->interrupt(line);
}

uint16_t& IODevice::at(std::size_t addr) {
    return mem[addr];
}

void IODevice::ignoreRange(std::size_t first, std::size_t last) {
    ignoreFirst = first;
    ignoreLast  = last;
}
