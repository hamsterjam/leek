#include "assembler.hpp"

#include <cstdint>

#include "codes.hpp"

uint16_t assemble(Op::Code op, uint8_t op1, uint8_t op2, uint8_t op3) {
    uint16_t ret = 0;

    if (op == Op::RELP || op == Op::RELN) {
        op3 = op2;
        op2 = op1;
        op1 = 0;
    }

    if (op & 0x10) {
        // Long Op
        ret |= (op & 0xF) << 12;
        ret |= op1 << 8;
        ret |= op2 << 4;
        ret |= op3;
    }
    else {
        // Short Op
        ret |= op << 8;
        ret |= op1 << 4;
        ret |= op2;
    }

    return ret;
}
