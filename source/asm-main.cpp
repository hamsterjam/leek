#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdint>
#include <cctype>
#include <string>

#include "assembler.hpp"
#include "codes.hpp"

int main(int argc, char** argv) {
    const char* inName  = 0;
    const char* outName = 0;

    // Process flags
    for (int i = 1; i < argc; ++i) {
        if (argv[i][0] == '-') {
            // This is a flag
            switch (argv[i][1]) {
                case 'o':
                    outName = argv[i+1];
                    i += 1;
                    break;

                default:
                    std::cerr << "Unrecognised flag: " << argv[i] << std::endl;
                    return 1;
            }
        }
        else {
            // Else treat it as the input file
            inName = argv[i];
        }
    }

    std::fstream fin (inName,  std::ios::in );
    std::fstream fout(outName, std::ios::out | std::ios::binary);

    // For now just read a line and output the corresponding binary
    while (!fin.eof()) {
        std::string lineString;
        std::getline(fin, lineString);
        if (lineString == "") continue;
        std::stringstream line(lineString);

        std::string tok;
        line >> tok;

        uint16_t instr;
        if (tok == "LIT") {
            // Just handle LIT seperately so I can write some programs...
            line >> tok;
            instr = stoi(tok, 0, 16);
        }
        else {
            Op::Code code = Op::fromString(tok.c_str());

            uint8_t op[3] = {0};
            for (int i = 0; i < 3; ++i) {
                if (line.eof()) break;
                line >> tok;

                char peek = tok[0];
                if (isdigit(peek)) {
                    op[i] = std::stoi(tok);
                }
                else {
                    switch (peek) {
                        case 'r':
                            op[i] = Reg::fromString(tok.c_str() + 1);
                            break;
                        case 'f':
                            op[i] = Flag::fromString(tok.c_str() + 1);
                            break;
                        default:
                            std::cerr << "Unexpected '" << peek << "' character" << std::endl;
                            return 1;
                    }
                }
            }
            instr = assemble(code, op[0], op[1], op[2]);
        }

        uint8_t instrLo = instr & ((1 << 8) - 1);
        uint8_t instrHi = instr >> 8;

        fout.put(instrHi);
        fout.put(instrLo);
    }
}
