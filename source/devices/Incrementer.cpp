#include "devices/Incrementer.hpp"

#include "VirtualMachine.hpp"

void Incrementer::onWake() {
    at(0) += 1;
    interrupt();
}
