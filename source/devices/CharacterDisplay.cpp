#include "devices/CharacterDisplay.hpp"

#include <cstdint>
#include <cstdlib>
#include <iostream>

const uint16_t CONTROLL = 0x07FF;

CharacterDisplay::CharacterDisplay() {
    ignoreRange(0, 2001);
}

void CharacterDisplay::onWake() {
    // If the controll hasn't been tripped, just do nothing
    if (!at(CONTROLL)) return;

    uint16_t code = at(CONTROLL);
    at(CONTROLL) = 0;

    switch(code) {
        case 1:
            // Reset input buffer
            for (int i = 0; i < 2000; ++i) at(i) = buffer[i];
            interrupt();
            break;
        case 2:
            // Set output buffer from input buffer
            for (int i = 0; i < 2000; ++i) buffer[i] = at(i);
            interrupt();
            redraw();
            break;
        default:
            return;
    }
}

void CharacterDisplay::redraw() {
    std::system("clear");
    for (int line = 0; line < 25; ++line) {
        for (int row = 0; row < 80; ++row) {
            uint16_t addr = row + (line * 80);

            char character =  buffer[addr] & 0x00FF;
            int  fgcolor   = (buffer[addr] & 0x0F00) >> 8;
            int  bgcolor   = (buffer[addr] & 0xF000) >> 12;

            if (fgcolor >= 8) fgcolor += 52;
            if (bgcolor >= 8) bgcolor += 52;

            fgcolor += 30;
            bgcolor += 40;

            if (character == 0) character = ' ';

            std::cout << "\e[" << fgcolor << "m";
            std::cout << "\e[" << bgcolor << "m";
            std::cout << character;
        }
        std::cout << std::endl;
    }
    std::cout << "\e[0m" << std::flush;
}
