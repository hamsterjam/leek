#include "devices/SimpleDisplay.hpp"
#include "VirtualMachine.hpp"

#include <iostream>

void SimpleDisplay::onWake() {
    std::cout << at(0) << std::endl;
    interrupt();
}
