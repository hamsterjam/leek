#include "VirtualMachine.hpp"
#include "IODevice.hpp"
#include "codes.hpp"

#include <cstdint>
#include <atomic>
#include <istream>
#include <iomanip>
#include <stdexcept>
#include <utility>

void VirtualMachine::execute(uint16_t op) {
    // Break the instruction into 4 nybbles
    uint8_t nyb[4];
    nyb[0] = (op & 0x000F) >> 0x0;
    nyb[1] = (op & 0x00F0) >> 0x4;
    nyb[2] = (op & 0x0F00) >> 0x8;
    nyb[3] = (op & 0xF000) >> 0xC;

    // Select the inputs and outputs (refer to the specification)
    uint16_t  inA, inB, opcode;
    uint16_t* out;
    bool memWrite   = false;
    bool stateFlags = false;
    if (nyb[3] != 0) {
        // Long Operation
        if (nyb[3] & 0x8) {
            // Arith
            inA = reg[nyb[2]];
            inB = reg[nyb[1]];
            stateFlags = true;
        }
        else if (!(nyb[3] & 0x4)) {
            // I-Arith
            inA = reg[nyb[2]];
            inB = nyb[1];
            if (nyb[3] == (Op::ROTI & 0xF)) stateFlags = true;
        }
        else if (!(nyb[3] & 0x2)) {
            // S-Arith
            inA = reg[nyb[2]];
            inB = reg[nyb[1]];
        }
        else {
            // R-Arith
            inA = (nyb[2] << 4) | nyb[1];
            inB = reg[Reg::PC];
        }
        out = &reg[nyb[0]];
        opcode  = 0x10 | nyb[3];
    }
    else {
        // Short Operation
        if ((nyb[2] & 0xC) == 0x0) {
            // Misc
            inA = reg[nyb[1]];
            out = &reg[nyb[0]];
            if (nyb[2] == Op::NOT) stateFlags = true;
        }
        else if ((nyb[2] & 0xC) == 0x4) {
            // Flag
            inA = nyb[1];
            out = &reg[nyb[0]];
        }
        else if ((nyb[2] & 0xC) == 0x8) {
            // Write
            inA = reg[nyb[1]];
            out = &mem[reg[nyb[0]]];
            memWrite = true;
        }
        else {
            // Read
            inA = mem[reg[nyb[1]]];
            out = &reg[nyb[0]];
        }
        inB = *out;
        opcode = nyb[2];
    }

    switch (opcode) {
        //
        // Arithmetic
        //

        // Additions
        case (Op::INC):
            *out = inA + inB;
            break;

        case (Op::ADD):
            *out = inA + inB;
            setFlag(Flag::CARRY, *out < inA);
            setFlag(Flag::OVER, ~(inA ^ inB) & (*out ^ inA) & 0x8000);
            break;

        case (Op::ADC):
            *out = inA + inB + (peekFlag(Flag::CARRY) ? 1 : 0);
            setFlag(Flag::CARRY, *out < inA);
            break;

        case (Op::RELP):
            *out = inB + inA;
            break;

        // Subtractions
        case (Op::DEC):
            *out = inA - inB;
            break;

        case (Op::SUB):
            *out = inA - inB;
            setFlag(Flag::CARRY, *out > inA);
            setFlag(Flag::OVER, ~(inA ^ ~inB) & (*out ^ inA) & 0x8000);
            break;

        case (Op::SBB):
            *out = inA - inB - (peekFlag(Flag::CARRY) ? 1 : 0);
            setFlag(Flag::CARRY, *out > inA);
            break;

        case (Op::RELN):
            *out = inB - inA;
            break;

        // Rotations
        case (Op::ROT):
        case (Op::ROTI):
            {
                inB %= 16;
                uint16_t mask = (1 << inB) - 1;
                *out  = (inA << inB) & ~mask;
                *out |= (inA >> 16 - inB) &  mask;
            }
            break;

        // Other
        case (Op::MUL):
            //TODO// Make this take multiple cycles?
            {
                uint32_t res = (uint32_t) inA * (uint32_t) inB;
                *out = res & 0xFFFF;
                reg[Reg::AUX] = (res & 0xFFFF0000) >> 16;
            }
            break;

        case (Op::DIV):
            //TODO// Make this take multiple cycles?
            {
                uint32_t divisor = reg[Reg::AUX] << 16 | inA;
                *out = divisor / inB;
                reg[Reg::AUX] = divisor % inB;
            }
            break;

        // Bitwise
        case (Op::OR):
            *out = inA | inB;
            break;

        case (Op::AND):
            *out = inA & inB;
            break;

        case (Op::XOR):
            *out = inA ^ inB;
            break;

        case (Op::NOT):
            *out = ~inA;
            break;

        //
        // Flags
        //

        case (Op::PRED):
            if (!(inB & (1 << inA))) reg[Reg::PC] += 1;
            break;

        case (Op::SET):
            *out |= 1 << inA;
            break;

        case (Op::CLR):
            *out &= ~(1 << inA);
            break;

        case (Op::TOG):
            *out ^= 1 << inA;
            break;

        //
        // Memory
        //

        case (Op::LOAD):
        case (Op::STORE):
        case (Op::MOV):
            *out = inA;
            break;

        case (Op::LOADL):
            *out = inA & 0x00FF;
            break;
        case (Op::STOREL):
            *out = (inB & 0xFF00) | (inA & 0x00FF);
            break;

        case (Op::LOADU):
            *out = (inA & 0xFF00) >> 8;
            break;
        case (Op::STOREU):
            *out = (inB & 0xFF00) | ((inA & 0xFF00) >> 8);
            break;

        case (Op::POP):
            *out = inA;
            reg[Reg::SP] -= 1;
            break;

        case (Op::PUSH):
            reg[Reg::SP] += 1;
            out = &mem[reg[Reg::SP]];
            *out = inA;
            break;

        //
        // Interrupts
        //

        case (Op::WAIT):
            if (peekFlag(Flag::IC)) {
                std::unique_lock<std::mutex> lk(intrMu);
                intrCv.wait(lk, [this]{return intrAny;});
                lk.unlock();
            }
            break;

        case (Op::INTER):
            {
                std::lock_guard<std::mutex> lk(intrMu);
                softIntr = true;
                intrAny = true;
            }
            intrCv.notify_all();
            break;
    }

    // Set fZERO and fNEG
    if (stateFlags) {
        setFlag(Flag::ZERO, *out == 0);
        setFlag(Flag::NEG,  *out & 0x8000);
    }

    // Reset rZERO back to 0
    reg[Reg::ZERO] = 0;

    // Signal the I/O Device if we wrote to its memory
    if (memWrite) {
        uint16_t addr = reg[nyb[0]];
        auto iter = devs.upper_bound(reg[nyb[0]]);
        if (iter != devs.end()) {
            IODevice* dev = iter->second;
            if (dev) {
                addr -= dev->mem - mem;
                dev->alert(addr);
            }
        }
    }
}

void VirtualMachine::tick() {
    // Check if any interrupts occured
    bool handleIntr = false;
    {
        std::lock_guard<std::mutex> lk(intrMu);
        if (intrAny) {
            handleIntr = true;

            // Set interupt signal flags for hardware interrupts
            for (int i = 0; i < 0x10; ++i) {
                if (hardIntr[i]) setFlag((Flag::Code) (Flag::IS0 + i), true);
                hardIntr[i] = false;
            }
            // Set interrupt signal flags for software interrupts
            if (softIntr) setFlag(Flag::ISS, true);
            softIntr = false;

            intrAny = false;
        }
    }

    // Process the interrupts
    if (handleIntr && peekFlag(Flag::IC)) {
        // If the control flag is set, disable it
        setFlag(Flag::IC, false);
        // Push PC to the stack
        reg[Reg::SP] += 1;
        mem[reg[Reg::SP]] = reg[Reg::PC];
        // Set PC to IP
        reg[Reg::PC] = reg[Reg::IP];
    }

    // Execute the next command
    uint16_t& pc    = reg[Reg::PC];
    uint16_t  instr = mem[pc];
    pc += 1;
    execute(instr);
}

void VirtualMachine::interrupt(uint16_t line) {
    {
        std::lock_guard<std::mutex> lk(intrMu);
        hardIntr[line] = true;
        intrAny = true;
    }
    intrCv.notify_all();
}

uint16_t VirtualMachine::peekReg(Reg::Code regID) {
    return reg[regID];
}

uint16_t VirtualMachine::peekMem(uint16_t addr) {
    return mem[addr];
}

bool VirtualMachine::peekFlag(Flag::Code flag) {
    return reg[Reg::FLG] & (0x1 << flag);
}

void VirtualMachine::addDevice(IODevice& dev, uint16_t first, uint16_t last, uint16_t line) {
    // Check if a device overlaps this range
    if (devs.upper_bound(first) != devs.lower_bound(last)) {
        throw std::invalid_argument("VirtualMachine::addDevice");
    }

    auto firstPos = devs.find(first);
    if (firstPos == devs.end()) devs[first] = 0;
    devs[last]  = &dev;

    dev.vm   = this;
    dev.mem  = mem + first;
    dev.line = line;
}

void VirtualMachine::initMem(std::istream& in) {
    for (uint16_t i = reg[Reg::PC]; !in.eof(); ++i) {
        char hi, lo;
        in.get(hi);
        in.get(lo);
        uint16_t val = (unsigned char) hi << 8 | (unsigned char) lo;
        mem[i] = val;
    }
}

void VirtualMachine::initMem(std::initializer_list<uint16_t> in) {
    uint16_t i = reg[Reg::PC];
    for (uint16_t instr : in) {
        mem[i] = instr;
        i += 1;
    }
}

void VirtualMachine::setFlag(Flag::Code flag, bool val) {
    uint16_t& flg = reg[Reg::FLG];
    uint16_t mask = 0x1 << flag;
    if (val) {
        flg |= mask;
    }
    else {
        flg &= ~mask;
    }
}
