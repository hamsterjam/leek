#include "codes.hpp"

#include <cstdlib> // free, atoi
#include <cstring> // strdup, strcmp
#include <cctype>  // toupper, isdigit

// I really can't think of a better way to implement this...
Op::Code Op::fromString(const char* str) {
    char* test = strdup(str);
    for (char* p = test; *p; ++p) *p = toupper(*p);

    Op::Code ret = Op::INVALID;

    // Store Operations

    // Misc
    if      (!strcmp(test, "MOV"))    ret = Op::MOV;
    else if (!strcmp(test, "NOT"))    ret = Op::NOT;
    else if (!strcmp(test, "INTER"))  ret = Op::INTER;
    else if (!strcmp(test, "WAIT"))   ret = Op::WAIT;
    // Flag
    else if (!strcmp(test, "PRED"))   ret = Op::PRED;
    else if (!strcmp(test, "SET"))    ret = Op::SET;
    else if (!strcmp(test, "CLR"))    ret = Op::CLR;
    else if (!strcmp(test, "TOG"))    ret = Op::TOG;
    // Write
    else if (!strcmp(test, "STORE"))  ret = Op::STORE;
    else if (!strcmp(test, "STOREL")) ret = Op::STOREL;
    else if (!strcmp(test, "STOREU")) ret = Op::STOREU;
    else if (!strcmp(test, "PUSH"))   ret = Op::PUSH;
    // Read
    else if (!strcmp(test, "LOAD"))   ret = Op::LOAD;
    else if (!strcmp(test, "LOADL"))  ret = Op::LOADL;
    else if (!strcmp(test, "LOADU"))  ret = Op::LOADU;
    else if (!strcmp(test, "POP"))    ret = Op::POP;

    // Long Operations

    // I-Arith
    else if (!strcmp(test, "INC"))    ret = Op::INC;
    else if (!strcmp(test, "DEC"))    ret = Op::DEC;
    else if (!strcmp(test, "ROTI"))   ret = Op::ROTI;
    // S-Arith
    else if (!strcmp(test, "MUL"))    ret = Op::MUL;
    else if (!strcmp(test, "DIV"))    ret = Op::DIV;
    // R-Arith
    else if (!strcmp(test, "REL+"))   ret = Op::RELP;
    else if (!strcmp(test, "REL-"))   ret = Op::RELN;
    // Arith
    else if (!strcmp(test, "ADD"))    ret = Op::ADD;
    else if (!strcmp(test, "ADC"))    ret = Op::ADC;
    else if (!strcmp(test, "SUB"))    ret = Op::SUB;
    else if (!strcmp(test, "SBB"))    ret = Op::SBB;
    else if (!strcmp(test, "ROT"))    ret = Op::ROT;
    else if (!strcmp(test, "OR"))     ret = Op::OR;
    else if (!strcmp(test, "AND"))    ret = Op::AND;
    else if (!strcmp(test, "XOR"))    ret = Op::XOR;

    free(test);
    return ret;
}

Reg::Code Reg::fromString(const char* str) {
    char* test = strdup(str);
    for (char* p = test; *p; ++p) *p = toupper(*p);

    Reg::Code ret = Reg::INVALID;

    if (isdigit(test[0])) {
        // Parse as a number
        ret = (Reg::Code) atoi(test);
    }
    else {
        // Parse as a name
        if      (!strcmp(test, "ZERO")) ret = Reg::ZERO;
        else if (!strcmp(test, "AUX"))  ret = Reg::AUX;
        else if (!strcmp(test, "IP"))   ret = Reg::IP;
        else if (!strcmp(test, "FLG"))  ret = Reg::FLG;
        else if (!strcmp(test, "SP"))   ret = Reg::SP;
        else if (!strcmp(test, "PC"))   ret = Reg::PC;
    }

    free(test);
    return ret;
}

Flag::Code Flag::fromString(const char* str) {
    char* test = strdup(str);
    for (char* p = test; *p; ++p) *p = toupper(*p);

    Flag::Code ret = Flag::INVALID;

    if (isdigit(test[0])) {
        // Parse as a number
        ret = (Flag::Code) atoi(test);
    }
    else {
        // Parse as a name
        if      (!strcmp(test, "ZERO"))  ret = Flag::ZERO;
        else if (!strcmp(test, "NEG"))   ret = Flag::NEG;
        else if (!strcmp(test, "CARRY")) ret = Flag::CARRY;
        else if (!strcmp(test, "OVER"))  ret = Flag::OVER;
        else if (!strcmp(test, "IC"))    ret = Flag::IC;
        else if (!strcmp(test, "ISS"))   ret = Flag::ISS;
        else if (!strcmp(test, "IS0"))   ret = Flag::IS0;
        else if (!strcmp(test, "IS1"))   ret = Flag::IS1;
        else if (!strcmp(test, "IS2"))   ret = Flag::IS2;
        else if (!strcmp(test, "IS3"))   ret = Flag::IS3;
        else if (!strcmp(test, "IS4"))   ret = Flag::IS4;
        else if (!strcmp(test, "IS5"))   ret = Flag::IS5;
        else if (!strcmp(test, "IS6"))   ret = Flag::IS6;
        else if (!strcmp(test, "IS7"))   ret = Flag::IS7;
    }

    free(test);
    return ret;
}
