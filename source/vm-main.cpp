#include <iostream>
#include <fstream>
#include <cstdint>

#include "VirtualMachine.hpp"
#include "devices/CharacterDisplay.hpp"
#include "codes.hpp"

VirtualMachine   vm;
CharacterDisplay cd;

void normalMode();
void interactiveMode();

int main(int argc, char** argv) {
    bool interactive = false;
    const char* filename = 0;

    // Process flags
    for (int i = 1; i < argc; ++i) {
        if (argv[i][0] == '-') {
            // This is a flag
            switch (argv[i][1]) {
                case 'i':
                    interactive = true;
                    break;
                case 'f':
                    filename = argv[i+1];
                    ++i;
                    break;
            }
        }
        else {
            std::cerr << "Unexpected argument: " << argv[i] << std::endl;
            return 1;
        }
    }

    // Setup the virtual machine
    vm.addDevice(cd, 0xD000, 0xD800, 0);

    if (filename) {
        std::fstream fin(filename, std::fstream::in);
        vm.initMem(fin);
    }

    if (interactive) interactiveMode();
    else             normalMode();

    return 0;
}

void normalMode() {
    while (1) {
        uint16_t oldPC = vm.peekReg(Reg::PC);
        vm.tick();
        if (vm.peekReg(Reg::PC) == oldPC) break;
    }
}

void interactiveMode() {
    bool shouldExit = false;
    while (!shouldExit) {
        // First character defines the action
        switch (std::cin.get()) {
            case 'p':
                {
                    // print
                    uint16_t reg;
                    std::cin >> std::hex >> reg;
                    std::cout << std::hex << vm.peekReg((Reg::Code) reg) << std::endl;
                }
                break;
            case 'e':
                {
                    // execute
                    uint16_t instr;
                    std::cin >> std::hex >> instr;
                    vm.execute(instr);
                }
                break;
            case 't':
                // tick
                vm.tick();
                break;
            case 'c':
                // current operation
                std::cout << std::hex << vm.peekMem(vm.peekReg(Reg::PC)) << std::endl;
                break;
            case 'q':
                // quit
                shouldExit = true;
                break;
        }
    }
}

